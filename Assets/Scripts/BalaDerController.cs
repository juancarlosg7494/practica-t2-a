using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaDerController : MonoBehaviour
{
    public float velocityX = 5;

    private Rigidbody2D rb;
    private GameController _game;
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        _game = FindObjectOfType<GameController>();
        Destroy(this.gameObject, 3);
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(velocityX, rb.velocity.y);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag != "Jugador")
            Destroy(this.gameObject);

        if (other.gameObject.CompareTag("Enemigo"))
        {
            Destroy(other.gameObject);
            _game.PlusScore(10);
            Debug.Log(_game.GetScore());
        }
        
    }
}
