using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public Text scoreText;
    public Text bronzeText;
    public Text silverText;
    public Text goldText;
    
    
    private int _score = 0;
    private int _bronze = 0;
    private int _silver = 0;
    private int _gold = 0;
    

    private void Start()
    {
        scoreText.text = "Score: " + _score;
        
    }


    public int GetScore()
    {
        return _score;
    }

    public void PlusScore(int score)
    {
        _score += score;
        scoreText.text = "Score: " + _score;
    }

    public int GetBronze()
    {
        return _bronze;
    }

    public void PlusBronze()
    {
        _bronze += 1;
        bronzeText.text = "Moneda Bronce: " + _bronze;
    }

    public int GetSilver()
    {
        return _silver;
    }

    public void PlusSilver()
    {
        _silver += 1;
        silverText.text = "Moneda Plata: " + _silver;
    }

    public int GetGold()
    {
        return _gold;
    }

    public void PlusGold()
    {
        _gold += 1;
        goldText.text = "Moneda Oro: " + _gold;
    }


    
}