using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatoController : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator animator;
    private SpriteRenderer sr;
    public float velocity  = 5;
    public float jump = 30;
    public GameObject rightBullet;
    public GameObject leftBullet;
    private GameController _game;
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        _game = FindObjectOfType<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetInteger("Estado", 0);

        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.velocity = new Vector2(velocity, rb.velocity.y); 
            sr.flipX = false;
            animator.SetInteger("Estado",1); 
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.velocity = new Vector2(-velocity, rb.velocity.y); 
            sr.flipX = true; 
            animator.SetInteger("Estado",1); 
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            rb.AddForce(Vector2.up * jump, ForceMode2D.Impulse);
            animator.SetInteger("Estado",2); 
        }   
        if (Input.GetKey(KeyCode.X))
        {
            animator.SetInteger("Estado",3); 
        }

        if (Input.GetKeyUp(KeyCode.C))
        {
            var bullet = sr.flipX ? leftBullet : rightBullet;
            var position = new Vector2(transform.position.x, transform.position.y);
            var rotation = rightBullet.transform.rotation;
            Instantiate(bullet, position, rotation);
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Oro"))
        {
            Destroy(collider.gameObject);
            _game.PlusGold();
        }
        if (collider.gameObject.CompareTag("Plata"))
        {
            Destroy(collider.gameObject);
            _game.PlusSilver();
        }
        if (collider.gameObject.CompareTag("Bonce"))
        {
            Destroy(collider.gameObject);
            _game.PlusBronze();
        }
    }
}
